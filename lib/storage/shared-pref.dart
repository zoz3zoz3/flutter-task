import "package:shared_preferences/shared_preferences.dart";

import "../helpers/loger.dart";

class SharedPref {
  static Future<String> read(String key) async {
    DateTime tick = DateTime.now();
    SharedPreferences instance = await SharedPreferences.getInstance();
    DateTime tack = DateTime.now();
    Logger.debug(
        "took to read $key " + tack.difference(tick).inMilliseconds.toString());
    return instance.getString(key);
  }

  static Future<void> write(String key, String value) async {
    SharedPreferences instance = await SharedPreferences.getInstance();
    instance.setString(key, value);
  }
}
