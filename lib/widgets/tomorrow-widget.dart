import 'package:flutter/material.dart';
import '../models/day.dart';
import '../models/period.dart';
import 'weather-icon-widget.dart';
import 'temp-widget.dart';
import 'local-text.dart';

class TomorrowWidget extends StatelessWidget {
  final Day day;
  final Period currentPeriod;
  TomorrowWidget(this.day, this.currentPeriod);
  @override
  Widget build(BuildContext context) {
    final mq = MediaQuery.of(context).size;

    bool isLandscape = MediaQuery.of(context).orientation.toString() ==
        'Orientation.landscape';
    return FittedBox(
      fit: BoxFit.contain,
      child: Container(
        // color: Colors.white12,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              // color: Colors.amber,
              height: isLandscape ? mq.height * 0.1 : mq.height * 0.05,
              width: isLandscape ? mq.width * 0.5 : mq.width * 0.35,
              padding: isLandscape ? EdgeInsets.all(0) : EdgeInsets.all(5),
              child: FittedBox(
                child: LocalText(
                  'Tomorrow',
                  style: isLandscape
                      ? Theme.of(context).textTheme.headline1
                      : Theme.of(context).textTheme.headline3,
                ),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  children: [
                    Container(
                      // color: Colors.white12,
                      padding: isLandscape
                          ? const EdgeInsets.all(10)
                          : const EdgeInsets.all(10),
                      height: isLandscape ? mq.height * 0.1 : mq.height * 0.05,
                      width: isLandscape ? mq.width * 0.2 : mq.width * 0.4,
                      child: TempWidget(day.dayPeriods.first.temp.toString()),
                    ),
                    Container(
                      // color: Colors.white12,
                      padding: isLandscape
                          ? const EdgeInsets.all(10)
                          : const EdgeInsets.all(0),
                      height: isLandscape ? mq.height * 0.1 : mq.height * 0.05,
                      width: isLandscape ? mq.width * 0.2 : mq.width * 0.4,
                      child: Center(
                        child: LocalText(
                          day.dayPeriods.first.weather.first.weatherParameters,
                          style: Theme.of(context).textTheme.headline3,
                        ),
                      ),
                    ),
                  ],
                ),
                Container(
                  // color: Colors.amber,
                  height: isLandscape ? mq.height * 0.15 : mq.height * 0.1,
                  width: isLandscape ? mq.width * 0.1 : mq.width * 0.3,
                  padding: isLandscape ? EdgeInsets.all(0) : EdgeInsets.all(5),
                  child: WeatherIcon(currentPeriod),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
