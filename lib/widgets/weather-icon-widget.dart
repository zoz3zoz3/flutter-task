import 'package:flutter/material.dart';
import '../models/period.dart';

class WeatherIcon extends StatelessWidget {
  final Period currenPeriod;
  WeatherIcon(this.currenPeriod);

  @override
  Widget build(BuildContext context) {
    final assetpath = getIconName(currenPeriod);
    return FittedBox(
      fit: BoxFit.contain,
      child: Image.asset(assetpath),
    );
  }

  // ignore: missing_return
  String getIconName(Period period) {
    if (period.partOfTheDay == 'd') {
      if (period.weather.first.weatherParameters == 'Clouds') {
        return 'assets/icons/cloudy.png';
      }
      if (period.weather.first.weatherParameters == 'Clear') {
        return 'assets/icons/clear.png';
      }
      if (period.weather.first.weatherParameters == 'Rain') {
        return 'assets/icons/rain.png';
      }
      if (period.weather.first.weatherParameters == 'Snow') {
        return 'assets/icons/snowy.png';
      }
    }
    if (period.partOfTheDay == 'n') {
      if (period.weather.first.weatherParameters == 'Clouds') {
        return 'assets/icons/cloudy-windy-night.png';
      }
      if (period.weather.first.weatherParameters == 'Clear') {
        return 'assets/icons/clear-night.png';
      }
      if (period.weather.first.weatherParameters == 'Rain') {
        return 'assets/icons/rain-night.png';
      }
      if (period.weather.first.weatherParameters == 'Snow') {
        return 'assets/icons/snowy-night.png';
      }
    }
  }
}
