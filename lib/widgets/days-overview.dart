import 'package:flutter/material.dart';
import '../models/day.dart';
import './day-row.dart';

class DaysOverView extends StatelessWidget {
  final List<Day> days;
  DaysOverView(this.days);
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemBuilder: (ctx, i) => DayRow(days[i]),
      itemCount: days.length,
    );
  }
}
