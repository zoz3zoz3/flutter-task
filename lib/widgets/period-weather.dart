import 'package:flutter/material.dart';
import '../models/period.dart';
import 'weather-icon-widget.dart';
import 'temp-widget.dart';

class PeriodWether extends StatelessWidget {
  final Period period;
  PeriodWether(this.period);
  @override
  Widget build(BuildContext context) {
    bool isLandscape = MediaQuery.of(context).orientation.toString() ==
        'Orientation.landscape';
    return Container(
      width: MediaQuery.of(context).size.width * 0.25,
      height: MediaQuery.of(context).size.height * 0.1,
      child: Column(
        children: [
          Expanded(
            flex: 1,
            child: period.partOfTheDay == 'd'
                ? Container(
                    // color: Colors.white12,
                    margin: EdgeInsets.only(top: 5),
                    child: Text(
                      period.periodDate.hour.toString() + ' AM',
                      style: Theme.of(context).textTheme.bodyText1,
                    ),
                  )
                : Container(
                    // color: Colors.white12,
                    margin: EdgeInsets.only(top: 5),
                    child: Text(
                      (period.periodDate.hour - 12).abs().toString() + ' PM',
                      style: Theme.of(context).textTheme.bodyText1,
                    ),
                  ),
          ),
          Expanded(
            flex: 1,
            child: Container(
              // color: Colors.white12,
              child: WeatherIcon(period),
            ),
          ),
          Expanded(
            flex: 1,
            child: Container(
                // color: Colors.white12,
                padding: isLandscape ? EdgeInsets.all(2) : EdgeInsets.all(7),
                child: TempWidget(period.temp.toString().substring(0, 2))),
          ),
          Expanded(
            flex: 1,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(period.humidity.toString() + '%'),
                Container(
                  // color: Colors.white12,
                  padding: isLandscape ? EdgeInsets.all(2) : EdgeInsets.all(7),
                  child: FittedBox(
                    fit: BoxFit.contain,
                    child: Image.asset('assets/icons/humidity.png'),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
