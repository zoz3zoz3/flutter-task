import 'package:flutter/material.dart';

class InfoCard extends StatelessWidget {
  final String iconPath;
  final Widget name;
  final Widget value;

  InfoCard({this.iconPath, this.name, this.value});
  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width * 0.4,
      child: Card(
        elevation: 10,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15.0),
        ),
        child: Column(
          children: [
            Expanded(
              flex: 1,
              child: Container(
                width: MediaQuery.of(context).size.width * 0.39,

                // color: Colors.black12,
                child: Center(
                  child: FittedBox(
                    child: name,
                  ),
                ),
              ),
            ),
            Expanded(
              flex: 2,
              child: Container(
                width: MediaQuery.of(context).size.width * 0.39,

                // color: Colors.black26,
                child: FittedBox(
                  fit: BoxFit.contain,
                  child: Image.asset(iconPath),
                ),
              ),
            ),
            Expanded(
              flex: 1,
              child: Container(
                width: MediaQuery.of(context).size.width * 0.39,

                // color: Colors.black38,
                child: Center(
                  child: FittedBox(child: value),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
