import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_task/localizaion/demo_localization.dart';
import 'package:flutter_task/providers/settings.dart';
import '../models/period.dart';
import './info-card.dart';
import 'local-text.dart';

class WeatherInfo extends StatelessWidget {
  final Period period;
  WeatherInfo(this.period);
  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width * 0.98,
      child: ListView(
        scrollDirection: Axis.horizontal,
        children: [
          //wind speed
          InfoCard(
            iconPath: 'assets/icons/wind.png',
            name: LocalText(
              'Wind Speed',
              style: Theme.of(context).textTheme.headline1,
            ),
            value: SettingsProvider.staticUnit == 'metric'
                ? Text(
                    period.windSpeed.toString() +
                        DemoLocalizations.of(context)
                            .getTranslatedValue(' km/h'),
                    style: Theme.of(context).textTheme.bodyText1,
                  )
                : Text(
                    period.windSpeed.toString() +
                        DemoLocalizations.of(context)
                            .getTranslatedValue(' m/h'),
                    style: Theme.of(context).textTheme.bodyText1,
                  ),
          ),
          //clouds in percentage
          InfoCard(
            iconPath: 'assets/icons/cloudy.png',
            name: LocalText(
              'Cloudiness',
              style: Theme.of(context).textTheme.headline1,
            ),
            value: Text(
              period.cloudiness.toString() + ' %',
              style: Theme.of(context).textTheme.bodyText1,
            ),
          ),
          //p o r in percentage
          InfoCard(
            iconPath: 'assets/icons/rain.png',
            name: LocalText(
              'Probability of Rain',
              style: Theme.of(context).textTheme.headline1,
            ),
            value: Text(
              period.probabilityOfPrecipitation.toString() + ' %',
              style: Theme.of(context).textTheme.bodyText1,
            ),
          ),
          //presure in hpa (hecto Pascals)
          InfoCard(
            iconPath: 'assets/icons/atmospher.png',
            name: LocalText(
              'Atmosphere Presure',
              style: Theme.of(context).textTheme.headline1,
            ),
            value: Text(
              period.pressure.toString() +
                  DemoLocalizations.of(context).getTranslatedValue(' hPa'),
              style: Theme.of(context).textTheme.bodyText1,
            ),
          ),
          //visibility in meters
          InfoCard(
            iconPath: 'assets/icons/visibility.png',
            name: LocalText(
              'Visibility',
              style: Theme.of(context).textTheme.headline1,
            ),
            value: Text(
              period.visibility.toString() +
                  DemoLocalizations.of(context).getTranslatedValue(' m'),
              style: Theme.of(context).textTheme.bodyText1,
            ),
          ),
        ],
      ),
    );
  }
}
