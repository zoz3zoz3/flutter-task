import 'package:flutter/material.dart';
import 'package:flutter_task/localizaion/demo_localization.dart';
import 'package:provider/provider.dart';
import '../providers/settings.dart';
import '../models/day.dart';
import './period-weather.dart';
import './city-widget.dart';
import 'temp-widget.dart';
import 'weather-icon-widget.dart';
import 'local-text.dart';

class DaySummary extends StatelessWidget {
  final Day currentDay;
  DaySummary(this.currentDay);
  @override
  Widget build(BuildContext context) {
    final mq = MediaQuery.of(context).size;
    bool isLandscape = MediaQuery.of(context).orientation.toString() ==
        'Orientation.landscape';
    return Container(
      width: mq.width,
      height: isLandscape ? mq.height * 0.53 : mq.height * 0.4,
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15.0),
        ),
        child: Column(
          children: [
            Expanded(
              flex: 1,
              child: Row(
                children: [
                  Container(
                    // color: Colors.white24,
                    margin: EdgeInsets.only(left: mq.width * 0.02),
                    child: CityWidget(
                      cityName: 'Dubai',
                      datetime: currentDay.date,
                    ),
                  ),
                ],
              ),
            ),
            Expanded(
              flex: 1,
              child: Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      flex: 1,
                      child: Container(
                        child: Row(
                          children: [
                            Container(
                              // color: Colors.white12,
                              padding: isLandscape
                                  ? EdgeInsets.all(10)
                                  : EdgeInsets.all(0),
                              margin: isLandscape
                                  ? EdgeInsets.only(left: mq.width * 0.01)
                                  : EdgeInsets.only(left: mq.width * 0.02),
                              width: mq.width * 0.2,
                              child: TempWidget(
                                  currentDay.dayPeriods.first.temp.toString()),
                            ),
                            Container(
                              // color: Colors.white12,
                              margin: EdgeInsets.only(left: mq.width * 0.02),
                              width: mq.width * 0.2,
                              child: WeatherIcon(currentDay.dayPeriods.first),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Expanded(
                      flex: 1,
                      child: Container(
                        // color: Colors.white12,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            Container(
                              // color: Colors.white12,
                              width: mq.width * 0.2,
                              height: isLandscape
                                  ? mq.height * 0.04
                                  : mq.height * 0.02,
                              margin: isLandscape
                                  ? EdgeInsets.all(0)
                                  : EdgeInsets.only(
                                      top: mq.height * 0.005,
                                      right: mq.width * 0.02,
                                    ),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                  Text(
                                    convertToTitleCase(currentDay
                                        .dayPeriods
                                        .first
                                        .weather
                                        .first
                                        .weatherDescription),
                                    style:
                                        Theme.of(context).textTheme.bodyText1,
                                  ),
                                ],
                              ),
                            ),
                            Container(
                              height: isLandscape
                                  ? mq.height * 0.04
                                  : mq.height * 0.02,
                              margin: isLandscape
                                  ? EdgeInsets.all(0)
                                  : EdgeInsets.only(
                                      top: mq.height * 0.005,
                                      right: mq.width * 0.02,
                                    ),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                  Text(
                                    currentDay.dayPeriods.first.tempMax
                                            .toString() +
                                        '°'
                                            ' / ' +
                                        currentDay.dayPeriods.last.tempMin
                                            .toString() +
                                        '°',
                                    style:
                                        Theme.of(context).textTheme.bodyText1,
                                  ),
                                ],
                              ),
                            ),
                            Container(
                              height: isLandscape
                                  ? mq.height * 0.04
                                  : mq.height * 0.02,
                              margin: isLandscape
                                  ? EdgeInsets.all(0)
                                  : EdgeInsets.only(
                                      top: mq.height * 0.005,
                                      right: mq.width * 0.02,
                                    ),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                  Text(
                                    DemoLocalizations.of(context)
                                            .getTranslatedValue('Feels Like ') +
                                        currentDay.dayPeriods.first.feelsLike
                                            .toString()
                                            .substring(0, 2) +
                                        '°C',
                                    style:
                                        Theme.of(context).textTheme.bodyText1,
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
            Expanded(
              flex: 2,
              child: Container(
                decoration: BoxDecoration(
                  border: Border(
                    top: BorderSide(
                      color: SettingsProvider.staticDarkMode
                          ? Colors.white
                          : Colors.black,
                    ),
                  ),
                ),
                // color: Colors.black45,
                child: ListView.builder(
                  scrollDirection: Axis.horizontal,
                  itemCount: currentDay.dayPeriods.length,
                  itemBuilder: (context, i) =>
                      PeriodWether(currentDay.dayPeriods[i]),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  String convertToTitleCase(String text) {
    if (text == null) {
      return null;
    }

    if (text.length <= 1) {
      return text.toUpperCase();
    }

    // Split string into multiple words
    final List<String> words = text.split(' ');

    // Capitalize first letter of each words
    final capitalizedWords = words.map((word) {
      if (word.trim().isNotEmpty) {
        final String firstLetter = word.trim().substring(0, 1).toUpperCase();
        final String remainingLetters = word.trim().substring(1);

        return '$firstLetter$remainingLetters';
      }
      return '';
    });

    // Join/Merge all words back to one String
    return capitalizedWords.join(' ');
  }
}
