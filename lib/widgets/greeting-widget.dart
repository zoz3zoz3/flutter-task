import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../providers/settings.dart';
import 'local-text.dart';

class Greetings extends StatelessWidget {
  final String pod;
  Greetings(this.pod);
  @override
  Widget build(BuildContext context) {
    return FittedBox(
      fit: BoxFit.contain,
      child: Consumer<SettingsProvider>(
        builder: (context, settings, widget) {
          return pod == 'n'
              ? LocalText(
                  'Good Night',
                  style: Theme.of(context).textTheme.headline1,
                )
              : LocalText(
                  'Good Day',
                  style: Theme.of(context).textTheme.headline1,
                );
        },
      ),
    );
  }
}
