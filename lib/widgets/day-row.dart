import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

import '../models/day.dart';

import '../providers/weather-forcast.dart';

import '../screens/day-screen.dart';

import 'local-text.dart';

class DayRow extends StatelessWidget {
  final Day day;
  DayRow(this.day);

  @override
  Widget build(BuildContext context) {
    day.setMorningNight();

    return InkWell(
      onTap: () {
        Provider.of<WeatherProvider>(context, listen: false).setSelected(day);
        Navigator.of(context).pushNamed(DayScreen.route);
      },
      child: Container(
        height: 50,
        margin: EdgeInsets.all(7.0),
        child: Row(
          children: [
            Expanded(
              child: LocalText(
                DateFormat.EEEE().format(day.date),
              ),
            ),
            Expanded(
              child: Text(
                DateFormat.yMd().format(day.date),
              ),
            ),
            Expanded(
              child: Row(
                children: [
                  Container(
                    // color: Colors.white12,
                    width: 20,
                    height: 20,
                    child: FittedBox(
                      fit: BoxFit.contain,
                      child: Image.asset('assets/icons/humidity.png'),
                    ),
                  ),
                  Text(
                    day.dayPeriods.first.humidity.toString() + '%',
                    style: Theme.of(context).textTheme.bodyText2,
                  ),
                ],
              ),
            ),
            Expanded(
              child: Row(
                children: [
                  Container(
                    // color: Colors.white12,
                    width: 30,
                    height: 30,
                    child: FittedBox(
                      fit: BoxFit.contain,
                      child: Image.asset('assets/icons/clear.png'),
                    ),
                  ),
                  Container(
                    // color: Colors.white12,
                    width: 30,
                    height: 30,
                    child: FittedBox(
                      fit: BoxFit.contain,
                      child: Image.asset('assets/icons/clear-night.png'),
                    ),
                  ),
                ],
              ),
            ),
            Expanded(
              child: day.morning.isEmpty || day.night.isEmpty
                  ? Text(
                      day.dayPeriods.first.tempMax.toString().substring(0, 2) +
                          '°'
                              ' / ' +
                          day.dayPeriods.last.tempMin
                              .toString()
                              .substring(0, 2) +
                          '°',
                      style: Theme.of(context).textTheme.bodyText2,
                    )
                  : Text(
                      day.morning.first.tempMax.toString().substring(0, 2) +
                          '°'
                              ' / ' +
                          day.night.last.tempMin.toString().substring(0, 2) +
                          '°',
                      style: Theme.of(context).textTheme.bodyText2,
                    ),
            )
          ],
        ),
      ),
    );
  }
}
