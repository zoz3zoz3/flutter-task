import '../localizaion/demo_localization.dart';
import 'package:flutter/material.dart';

class LocalText extends StatelessWidget {
  final String data;
  final TextStyle style;
  final TextAlign textAlign;
  LocalText(this.data, {this.style, this.textAlign});
  @override
  Widget build(BuildContext context) {
    return Text(
      DemoLocalizations.of(context).getTranslatedValue(data),
      style: style,
      textAlign: textAlign,
    );
  }
}
