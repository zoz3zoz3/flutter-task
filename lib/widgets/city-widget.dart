import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'local-text.dart';

class CityWidget extends StatelessWidget {
  final String cityName;
  final DateTime datetime;

  CityWidget({this.cityName, this.datetime});

  @override
  Widget build(BuildContext context) {
    final mq = MediaQuery.of(context).size;
    bool isLandscape = MediaQuery.of(context).orientation.toString() ==
        'Orientation.landscape';
    String dayName;
    if (datetime.day == DateTime.now().day) {
      dayName = 'Today';
    } else {
      dayName = DateFormat.EEEE().format(datetime);
    }
    return FittedBox(
      fit: BoxFit.contain,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            margin: EdgeInsets.only(bottom: 10),
            // color: Colors.white12,
            child: Row(
              children: [
                Icon(
                  Icons.location_on_outlined,
                  size: 40,
                ),
                LocalText(
                  cityName,
                  style: Theme.of(context).textTheme.headline6,
                ),
              ],
            ),
          ),
          dayName == 'Today'
              ? Container(
                  // width: isLandscape ? mq.width * 0.12 : mq.width * 0.1,
                  // height: isLandscape ? mq.height * 0.12 : mq.height * 0.1,
                  // color: Colors.white12,
                  child: LocalText(
                    dayName,
                    style: Theme.of(context).textTheme.headline2,
                  ),
                )
              : Container(),
          Container(
            // width: isLandscape ? mq.width * 0.12 : mq.width * 0.1,
            // height: isLandscape ? mq.height * 0.12 : mq.height * 0.1,
            // color: Colors.white12,
            child: Text(
              DateFormat.yMEd().format(datetime),
              style: Theme.of(context).textTheme.headline1,
            ),
          ),
        ],
      ),
    );
  }
}
