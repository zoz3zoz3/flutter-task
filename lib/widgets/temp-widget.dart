import 'package:flutter/material.dart';
import 'package:flutter_task/providers/settings.dart';

class TempWidget extends StatelessWidget {
  final String tempreture;
  TempWidget(this.tempreture);
  @override
  Widget build(BuildContext context) {
    return FittedBox(
      fit: BoxFit.contain,
      child: SettingsProvider.staticUnit == 'metric'
          ? Text(
              tempreture + '°C',
              style: Theme.of(context).textTheme.headline3,
            )
          : Text(
              tempreture + '°F',
              style: Theme.of(context).textTheme.headline3,
            ),
    );
  }
}
