import 'package:flutter/material.dart';
import 'package:flutter_task/models/period.dart';
import '../widgets/city-widget.dart';
import '../widgets/weather-icon-widget.dart';
import '../widgets/temp-widget.dart';
import '../widgets/greeting-widget.dart';
import '../models/city.dart';
import '../widgets/local-text.dart';

class Summary extends StatelessWidget {
  final City currentCity;
  final Period currentPeriod;

  Summary({this.currentCity, this.currentPeriod});
  @override
  Widget build(BuildContext context) {
    final mq = MediaQuery.of(context).size;
    bool isLandscape = MediaQuery.of(context).orientation.toString() ==
        'Orientation.landscape';
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15.0),
      ),
      elevation: 4,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Container(
                // color: Colors.white12,
                height: isLandscape ? mq.height * 0.15 : mq.height * 0.15,
                width: isLandscape ? mq.width * 0.2 : mq.width * 0.4,
                padding: isLandscape
                    ? const EdgeInsets.all(0)
                    : const EdgeInsets.all(10),
                child: CityWidget(
                  cityName: currentCity.name,
                  datetime: DateTime.now(),
                ),
              ),
              Column(
                children: [
                  Container(
                    // color: Colors.white12,
                    padding: isLandscape
                        ? const EdgeInsets.all(10)
                        : const EdgeInsets.all(10),
                    height: isLandscape ? mq.height * 0.1 : mq.height * 0.06,
                    width: isLandscape ? mq.width * 0.2 : mq.width * 0.4,
                    child: TempWidget(currentPeriod.temp.toString()),
                  ),
                  Container(
                    // color: Colors.white12,
                    padding: isLandscape
                        ? const EdgeInsets.all(10)
                        : const EdgeInsets.all(0),
                    height: isLandscape ? mq.height * 0.1 : mq.height * 0.06,
                    width: isLandscape ? mq.width * 0.2 : mq.width * 0.4,
                    child: Center(
                      child: LocalText(
                        currentPeriod.weather.first.weatherParameters,
                        style: Theme.of(context).textTheme.headline3,
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Container(
                // color: Colors.white12,
                padding: isLandscape
                    ? const EdgeInsets.all(10)
                    : const EdgeInsets.all(20),
                height: isLandscape ? mq.height * 0.15 : mq.height * 0.15,
                width: isLandscape ? mq.width * 0.2 : mq.width * 0.4,
                child: Greetings(currentPeriod.partOfTheDay),
              ),
              Container(
                // color: Colors.white12,
                padding: isLandscape
                    ? const EdgeInsets.all(0)
                    : const EdgeInsets.all(20),
                height: isLandscape ? mq.height * 0.15 : mq.height * 0.15,
                width: isLandscape ? mq.width * 0.2 : mq.width * 0.4,
                child: WeatherIcon(currentPeriod),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
