import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../screens/splash-screen.dart';
import '../screens/home-screen.dart';
import '../screens/day-screen.dart';

Map<String, Widget Function(BuildContext)> routes = {
  SplashScreen.route: (ctx) => SplashScreen(),
  HomeScreen.route: (ctx) => HomeScreen(),
  DayScreen.route: (ctx) => DayScreen()
};
