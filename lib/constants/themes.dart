import 'package:flutter/material.dart';

ThemeData theme = ThemeData.light().copyWith(
  // primaryColor: Colors.red,
  appBarTheme: AppBarTheme(color: Colors.black26),
  textTheme: TextTheme(
    headline6: TextStyle(
        fontSize: 45, color: Colors.black, fontWeight: FontWeight.bold),
    headline5: TextStyle(
        fontSize: 30, color: Colors.black, fontWeight: FontWeight.w700),
    headline4: TextStyle(
        fontSize: 25, color: Colors.black, fontWeight: FontWeight.w500),
    headline3: TextStyle(
        fontSize: 20, color: Colors.black, fontWeight: FontWeight.normal),
    headline2: TextStyle(
        fontSize: 17, color: Colors.black, fontWeight: FontWeight.normal),
    headline1: TextStyle(
        fontSize: 16, color: Colors.black, fontWeight: FontWeight.normal),
    bodyText1: TextStyle(
        fontSize: 14, color: Colors.black, fontWeight: FontWeight.normal),
    bodyText2: TextStyle(
        fontSize: 13, color: Colors.black, fontWeight: FontWeight.w300),
    subtitle1: TextStyle(
        fontSize: 12, color: Colors.black, fontWeight: FontWeight.w300),
    subtitle2: TextStyle(
        fontSize: 11, color: Colors.black, fontWeight: FontWeight.w300),
  ),
);

ThemeData darkTheme = ThemeData.dark().copyWith(
  textTheme: TextTheme(
    headline6: TextStyle(
        fontSize: 45, color: Colors.white, fontWeight: FontWeight.bold),
    headline5: TextStyle(
        fontSize: 30, color: Colors.white, fontWeight: FontWeight.w800),
    headline4: TextStyle(
        fontSize: 25, color: Colors.white, fontWeight: FontWeight.w500),
    headline3: TextStyle(
        fontSize: 20, color: Colors.white, fontWeight: FontWeight.normal),
    headline2: TextStyle(
        fontSize: 17, color: Colors.white, fontWeight: FontWeight.normal),
    headline1: TextStyle(
        fontSize: 16, color: Colors.white, fontWeight: FontWeight.normal),
    bodyText1: TextStyle(
        fontSize: 14, color: Colors.white, fontWeight: FontWeight.normal),
    bodyText2: TextStyle(
        fontSize: 13, color: Colors.white, fontWeight: FontWeight.w300),
    subtitle1: TextStyle(
        fontSize: 12, color: Colors.white, fontWeight: FontWeight.w300),
    subtitle2: TextStyle(
        fontSize: 11, color: Colors.white, fontWeight: FontWeight.w300),
  ),
);
