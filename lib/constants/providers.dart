import 'package:provider/provider.dart';
import 'package:provider/single_child_widget.dart';

import '../providers/weather-forcast.dart';
import '../providers/settings.dart';

List<SingleChildWidget> providers = [
  ChangeNotifierProvider.value(value: WeatherProvider()),
  ChangeNotifierProvider.value(value: SettingsProvider())
];
