import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';

class DemoLocalizations {
  Locale locale;

  DemoLocalizations(this.locale);

  static DemoLocalizations of(BuildContext context) {
    return Localizations.of<DemoLocalizations>(context, DemoLocalizations);
  }

  Map<dynamic, dynamic> localizedValue;

  Future<void> load() async {
    String jsonStringValue =
        await rootBundle.loadString('lib/lang/${locale.languageCode}.json');

    var jsonMappedValues = json.decode(jsonStringValue);

    localizedValue = jsonMappedValues.map((key, value) => MapEntry(key, value));
  }

  String getTranslatedValue(String key) {
    if (localizedValue[key] != null) {
      return localizedValue[key];
    } else
      return key;
  }

  static const LocalizationsDelegate<DemoLocalizations> delegate =
      DemoLocalizationsDelegate();
}

class DemoLocalizationsDelegate
    extends LocalizationsDelegate<DemoLocalizations> {
  const DemoLocalizationsDelegate();

  @override
  bool isSupported(Locale locale) => ['en', 'ar'].contains(locale.languageCode);

  @override
  Future<DemoLocalizations> load(Locale locale) async {
    DemoLocalizations demoLocalizations = DemoLocalizations(locale);
    await demoLocalizations.load();
    return demoLocalizations;
  }

  @override
  bool shouldReload(DemoLocalizationsDelegate old) => false;
}
