import 'dart:io';

import "package:flutter/material.dart";

import "package:dio/dio.dart";

import '../models/common/custom-error.dart';

class Request {
  static const BASE_URL =
      "http://api.openweathermap.org/data/2.5/forecast?id=292223&appid=d55f90385bcfa34e53536a71fa773c47";

  final bool authenticated;
  final Map<String, String> pathVariables;
  final Map<String, dynamic> queryParam;
  final CancelToken cancelToken;
  final Function(int i, int j) onReceivedProgress;
  final bool statusCheck;

  Request({
    this.authenticated = false,
    this.pathVariables,
    this.queryParam,
    this.cancelToken,
    this.onReceivedProgress,
    this.statusCheck = false,
  });

  Future<Options> _options({String contentType: "application/json"}) async {
    return Options(
      contentType: contentType,
      //headers: authenticated ? {"Authorization": await Auth.getJWt()} : null,
    );
  }

  Future<Response> get() async {
    try {
      Dio dio = Dio();
      Response response = await dio.get(
        BASE_URL,
        options: await _options(),
        queryParameters: queryParam,
        cancelToken: cancelToken,
        onReceiveProgress: onReceivedProgress,
      );
      if (statusCheck) CustomError.statusCheck(response);
      return response;
    } catch (e) {
      CustomError.networkErrorManager(e);
      return null;
    }
  }

  Future<Response> post({Map<String, dynamic> body}) async {
    try {
      Dio dio = Dio();
      Response response = await dio.post(BASE_URL,
          options: await _options(),
          queryParameters: queryParam,
          data: body,
          cancelToken: cancelToken,
          onReceiveProgress: onReceivedProgress);
      if (statusCheck) CustomError.statusCheck(response);
      return response;
    } catch (e) {
      CustomError.networkErrorManager(e);
      return null;
    }
  }

  Future<Response> patch({@required Map<String, dynamic> body}) async {
    try {
      Dio dio = Dio();
      Response response = await dio.patch(BASE_URL,
          options: await _options(),
          queryParameters: queryParam,
          data: body,
          cancelToken: cancelToken,
          onReceiveProgress: onReceivedProgress);
      if (statusCheck) CustomError.statusCheck(response);
      return response;
    } catch (e) {
      CustomError.networkErrorManager(e);
      return null;
    }
  }

  Future<Response> postFormData({
    Map<String, dynamic> body,
    Map<String, File> files,
  }) async {
    try {
      Dio dio = Dio();
      FormData formData = FormData.fromMap({
        ...body,
        ...filesToFormData(files),
      });

      Response response = await dio.post(BASE_URL,
          //options: await _options(),
          queryParameters: queryParam,
          cancelToken: cancelToken,
          data: formData);
      if (statusCheck) CustomError.statusCheck(response);
      return response;
    } catch (e) {
      print(e);
      CustomError.networkErrorManager(e);
      return null;
    }
  }

  Future<Response> delete() async {
    try {
      Dio dio = Dio();
      Response response = await dio.delete(
        BASE_URL,
        options: await _options(),
        queryParameters: queryParam,
        cancelToken: cancelToken,
      );
      if (statusCheck) CustomError.statusCheck(response);
      return response;
    } catch (e) {
      CustomError.networkErrorManager(e);
      return null;
    }
  }
}

Map<String, MultipartFile> filesToFormData(Map<String, File> files) {
  Map<String, MultipartFile> res = {};
  files.forEach((key, value) {
    if (value != null) res[key] = MultipartFile.fromFileSync(value.path);
  });
  return res;
}
