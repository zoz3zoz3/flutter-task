import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_task/helpers/loger.dart';
import 'package:provider/provider.dart';

import '../models/city.dart';
import '../models/day.dart';
import '../models/period.dart';

import '../providers/settings.dart';
import '../providers/weather-forcast.dart';

import '../widgets/tomorrow-widget.dart';
import '../widgets/days-overview.dart';
import '../widgets/summary-widget.dart';
import '../widgets/local-text.dart';

import './settings-screen.dart';

class HomeScreen extends StatefulWidget {
  static const String route = '/home';
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  List<Period> allPeriods;
  List<Day> alldays;
  City currentCity;
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final wf = Provider.of<WeatherProvider>(context, listen: true);
    allPeriods = wf.getallPeriods;
    currentCity = wf.currentCity;
    alldays = wf.getalldays;
    Day tomorrow = alldays
        .firstWhere((element) => element.date.day == DateTime.now().day + 1);
    Period currentPeriod = alldays.first.dayPeriods.firstWhere(
        (element) => element.periodDate.hour - DateTime.now().hour <= 2);
    final mq = MediaQuery.of(context).size;
    bool isLandscape = MediaQuery.of(context).orientation.toString() ==
        'Orientation.landscape';

    Logger.debug('dark mode');
    Logger.debug(SettingsProvider.staticDarkMode);
    Logger.debug('locale');
    Logger.debug(SettingsProvider.staticLocale);
    Logger.debug('unit');
    Logger.debug(SettingsProvider.staticUnit);

    return Scaffold(
      appBar: AppBar(
        title: LocalText('Weather'),
        actions: [
          IconButton(
            icon: Icon(Icons.settings),
            onPressed: () {
              Navigator.of(context).pushNamed(SettingsScreen.route);
            },
          )
        ],
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: isLandscape
                ? CrossAxisAlignment.start
                : CrossAxisAlignment.center,
            children: [
              if (isLandscape)
                Row(
                  children: [
                    Container(
                      width: mq.width * 0.5,
                      height: mq.height * 0.4,
                      child: Summary(
                          currentCity: currentCity,
                          currentPeriod: currentPeriod),
                    ),
                    Container(
                      width: mq.width * 0.5,
                      height: mq.height * 0.4,
                      child: Card(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(15.0),
                        ),
                        elevation: 4,
                        child: TomorrowWidget(tomorrow, currentPeriod),
                      ),
                    ),
                  ],
                ),
              if (!isLandscape)
                Container(
                  width: isLandscape ? mq.width / 2 : mq.width * 0.99,
                  height: isLandscape ? mq.height * 0.4 : mq.height * 0.33,
                  margin: EdgeInsets.only(
                    bottom: mq.height * 0.01,
                    left: mq.width * 0.01,
                    right: mq.width * 0.01,
                  ),
                  child: Summary(
                    currentCity: currentCity,
                    currentPeriod: currentPeriod,
                  ),
                ),
              if (!isLandscape)
                Container(
                  width: isLandscape ? mq.width / 2 : mq.width,
                  height: isLandscape ? mq.height * 0.4 : mq.height * 0.15,
                  margin: EdgeInsets.only(
                    bottom: mq.height * 0.01,
                    left: mq.width * 0.01,
                    right: mq.width * 0.01,
                  ),
                  child: Card(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15.0),
                    ),
                    elevation: 4,
                    child: TomorrowWidget(tomorrow, currentPeriod),
                  ),
                ),
              Container(
                width: mq.width,
                height: isLandscape ? mq.height * 0.45 : mq.height * 0.37,
                margin: EdgeInsets.only(
                  bottom: mq.height * 0.01,
                  left: mq.width * 0.01,
                  right: mq.width * 0.01,
                ),
                child: Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15.0),
                  ),
                  elevation: 4,
                  child: DaysOverView(alldays),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
