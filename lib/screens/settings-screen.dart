import 'package:flutter/material.dart';
import 'package:flutter_task/providers/settings.dart';
import 'package:flutter_task/providers/weather-forcast.dart';
import 'package:flutter_task/screens/home-screen.dart';
import 'package:provider/provider.dart';
import '../widgets/local-text.dart';

class SettingsScreen extends StatefulWidget {
  static const String route = '/settings-screen';
  @override
  _SettingsScreenState createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> {
  bool refresh = false;
  bool loading = false;
  @override
  Widget build(BuildContext context) {
    bool switchValue = SettingsProvider.staticDarkMode;
    String oldlang = SettingsProvider.staticLocale.languageCode;
    String oldunit = SettingsProvider.staticUnit;
    final mq = MediaQuery.of(context).size;
    final forcast = Provider.of<WeatherProvider>(context, listen: false);
    final settings = Provider.of<SettingsProvider>(context, listen: true);
    return WillPopScope(
      onWillPop: () {
        if (refresh) {
          Navigator.of(context).pushNamedAndRemoveUntil(
              HomeScreen.route, ModalRoute.withName(HomeScreen.route));
          return Future.value(true);
        } else {
          return Future.value(true);
        }
      },
      child: Scaffold(
        appBar: AppBar(
          title: LocalText('Settings'),
        ),
        body: SafeArea(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: loading
                ? Center(
                    child: CircularProgressIndicator(),
                  )
                : Column(
                    children: [
                      ListTile(
                        leading: Icon(Icons.language),
                        title: LocalText(
                          'Language',
                          style: Theme.of(context).textTheme.headline2,
                        ),
                        onTap: () {
                          showModalBottomSheet(
                            elevation: 10,
                            context: context,
                            builder: (context) {
                              return Container(
                                height:
                                    MediaQuery.of(context).size.height * 0.2,
                                child: SingleChildScrollView(
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Container(
                                        child: ListTile(
                                          // selected:
                                          //     oldlang == 'ar' ? true : false,s
                                          selected: settings.currlocale
                                                      .languageCode ==
                                                  'ar'
                                              ? true
                                              : false,
                                          selectedTileColor: Theme.of(context)
                                              .accentColor
                                              .withAlpha(75),
                                          title: LocalText(
                                            "Arabic",
                                            style: Theme.of(context)
                                                .textTheme
                                                .bodyText1,
                                          ),
                                          leading: Icon(Icons.arrow_right),
                                          onTap: () async {
                                            setState(() {
                                              refresh = true;
                                              loading = true;
                                            });
                                            Provider.of<SettingsProvider>(
                                                    context,
                                                    listen: false)
                                                .changeLocale(
                                              Locale('ar', 'SY'),
                                            );

                                            forcast
                                                .fetchData(
                                              refresh: true,
                                              unit: SettingsProvider.staticUnit,
                                              lang: 'ar',
                                              throwOnError: true,
                                              onError: (error) {
                                                print(error.message);
                                              },
                                            )
                                                .then((value) {
                                              setState(() {
                                                loading = false;
                                              });
                                            });
                                          },
                                        ),
                                      ),
                                      Divider(
                                        thickness: 2.0,
                                      ),
                                      Container(
                                        child: ListTile(
                                          selected: settings.currlocale
                                                      .languageCode ==
                                                  'en'
                                              ? true
                                              : false,
                                          selectedTileColor: Theme.of(context)
                                              .accentColor
                                              .withAlpha(75),
                                          title: LocalText(
                                            "English",
                                            style: Theme.of(context)
                                                .textTheme
                                                .bodyText1,
                                          ),
                                          leading: Icon(Icons.arrow_right),
                                          onTap: () async {
                                            setState(() {
                                              refresh = true;
                                              loading = true;
                                            });
                                            Provider.of<SettingsProvider>(
                                                    context,
                                                    listen: false)
                                                .changeLocale(
                                                    Locale('en', 'US'));
                                            forcast
                                                .fetchData(
                                              refresh: true,
                                              unit: SettingsProvider.staticUnit,
                                              lang: 'en',
                                              throwOnError: true,
                                              onError: (error) {
                                                print(error.message);
                                              },
                                            )
                                                .then((value) {
                                              setState(() {
                                                loading = false;
                                              });
                                            });
                                          },
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              );
                            },
                          );
                        },
                      ),
                      ListTile(
                        leading: Icon(Icons.tune_sharp),
                        title: LocalText(
                          'Units of measurement',
                          style: Theme.of(context).textTheme.headline2,
                        ),
                        onTap: () {
                          showModalBottomSheet(
                            elevation: 10,
                            context: context,
                            builder: (context) {
                              return Container(
                                height:
                                    MediaQuery.of(context).size.height * 0.2,
                                child: SingleChildScrollView(
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Container(
                                        child: ListTile(
                                          selected: settings.unit == 'metric'
                                              ? true
                                              : false,
                                          selectedTileColor: Theme.of(context)
                                              .accentColor
                                              .withAlpha(75),
                                          title: LocalText(
                                            "Celsius",
                                            style: Theme.of(context)
                                                .textTheme
                                                .bodyText1,
                                          ),
                                          leading: Icon(Icons.arrow_right),
                                          onTap: () async {
                                            setState(() {
                                              refresh = true;
                                              loading = true;
                                            });
                                            Provider.of<SettingsProvider>(
                                                    context,
                                                    listen: false)
                                                .changeUnit('metric');
                                            forcast
                                                .fetchData(
                                              refresh: true,
                                              unit: 'metric',
                                              lang: SettingsProvider
                                                  .staticLocale.languageCode,
                                              throwOnError: true,
                                              onError: (error) {
                                                print(error.message);
                                              },
                                            )
                                                .then((value) {
                                              setState(() {
                                                loading = false;
                                              });
                                            });
                                          },
                                        ),
                                      ),
                                      Divider(
                                        thickness: 2.0,
                                      ),
                                      Container(
                                        child: ListTile(
                                          selected: settings.unit == 'imperial'
                                              ? true
                                              : false,
                                          selectedTileColor: Theme.of(context)
                                              .accentColor
                                              .withAlpha(75),
                                          title: LocalText(
                                            "Fahrenheit",
                                            style: Theme.of(context)
                                                .textTheme
                                                .bodyText1,
                                          ),
                                          leading: Icon(Icons.arrow_right),
                                          onTap: () async {
                                            setState(() {
                                              refresh = true;
                                              loading = true;
                                            });
                                            Provider.of<SettingsProvider>(
                                                    context,
                                                    listen: false)
                                                .changeUnit('imperial');
                                            forcast
                                                .fetchData(
                                              refresh: true,
                                              unit: 'imperial',
                                              lang: SettingsProvider
                                                  .staticLocale.languageCode,
                                              throwOnError: true,
                                              onError: (error) {
                                                print(error.message);
                                              },
                                            )
                                                .then((value) {
                                              setState(() {
                                                loading = false;
                                              });
                                            });
                                          },
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              );
                            },
                          );
                        },
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            width: MediaQuery.of(context).size.width * 0.5,
                            // color: Colors.white12,
                            child: ListTile(
                              leading: Icon(Icons.nights_stay_rounded),
                              title: LocalText(
                                'Dark Mode',
                                style: Theme.of(context).textTheme.headline2,
                              ),
                            ),
                          ),
                          Container(
                            width: MediaQuery.of(context).size.width * 0.4,
                            // color: Colors.white12,
                            child: Center(
                              child: Switch(
                                value: switchValue,
                                onChanged: (value) {
                                  setState(() {
                                    switchValue = value;
                                    print(switchValue);
                                  });
                                  Provider.of<SettingsProvider>(context,
                                          listen: false)
                                      .changeTheme(value);
                                },
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
          ),
        ),
      ),
    );
  }
}
