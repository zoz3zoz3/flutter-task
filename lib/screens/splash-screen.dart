import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_task/models/day.dart';
import 'package:flutter_task/providers/settings.dart';
import 'package:provider/provider.dart';
import 'package:lottie/lottie.dart';
import '../providers/weather-forcast.dart';
import './home-screen.dart';

class SplashScreen extends StatefulWidget {
  static const String route = '/';

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  bool loading = true;

  @override
  void dispose() {
    super.dispose();
  }

  void appinit(BuildContext context) {
    final forcast = Provider.of<WeatherProvider>(context, listen: false);
    forcast
        .fetchData(
      unit: SettingsProvider.staticUnit,
      lang: SettingsProvider.staticLocale.languageCode,
      throwOnError: true,
    )
        .then((value) {
      setState(() {
        loading = false;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    if (loading) {
      appinit(context);
    }
    if (!loading) {
      Future.delayed(Duration(milliseconds: 2500)).then(
        (value) => Navigator.of(context).pushReplacementNamed(HomeScreen.route),
      );
    }

    return Scaffold(
      body: SafeArea(
        child: Center(
          child: Container(
            width: MediaQuery.of(context).size.width / 2,
            height: MediaQuery.of(context).size.height / 2,
            child: Lottie.asset('assets/animations/sun2.json'),
          ),
        ),
      ),
    );
  }
}
