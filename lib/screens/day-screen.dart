import 'package:flutter/material.dart';
import 'package:flutter_task/providers/weather-forcast.dart';
import 'package:provider/provider.dart';

import '../models/day.dart';

import '../widgets/weather-info.dart';
import '../widgets/day-summary.dart';
import '../widgets/local-text.dart';

class DayScreen extends StatefulWidget {
  static const String route = '/day-screen';

  @override
  _DayScreenState createState() => _DayScreenState();
}

class _DayScreenState extends State<DayScreen> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final Day day =
        Provider.of<WeatherProvider>(context, listen: false).theSlectedDay;

    day.findPeriod(DateTime.now());
    final mq = MediaQuery.of(context).size;
    bool isLandscape = MediaQuery.of(context).orientation.toString() ==
        'Orientation.landscape';
    return Scaffold(
      body: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Expanded(
              flex: 3,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  if (!isLandscape)
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          height:
                              isLandscape ? mq.height * 0.12 : mq.height * 0.15,
                          width: mq.width,
                          margin: EdgeInsets.only(bottom: mq.height * 0.01),
                          child: Center(
                            child: LocalText(
                              'Weather',
                              style: isLandscape
                                  ? Theme.of(context).textTheme.headline4
                                  : Theme.of(context).textTheme.headline5,
                            ),
                          ),
                        ),
                      ],
                    ),
                  DaySummary(day),
                ],
              ),
            ),
            Expanded(
              flex: isLandscape ? 2 : 1,
              child: WeatherInfo(day.dayPeriods.first),
            ),
          ],
        ),
      ),
    );
  }
}
