const bool LOGGING = true;
const bool ERROR_LOG = true;

class Logger {
  static void debug(var args, {bool show: true}) {
    if (LOGGING && show) print('\x1B[33m$args\x1B[0m');
  }

  static void logError(var args, {bool show: true}) {
    if (ERROR_LOG && show) print('\x1B[31m$args\x1B[0m');
  }

  static void screenBuilding(var screenName) {
    debug("Building $screenName");
  }
}
