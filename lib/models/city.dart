class City {
  int id;
  String name;
  dynamic lat;
  dynamic lon;
  String countryCode;
  dynamic shiftInSecondsFromUTC;

  City({
    this.id,
    this.name,
    this.countryCode,
    this.lat,
    this.lon,
    this.shiftInSecondsFromUTC,
  });

  City.fromJson(Map<String, dynamic> json) {
    this.id = json['id'];
    this.name = json['name'];
    this.countryCode = json['country'];
    this.shiftInSecondsFromUTC = json['timezone'];
    this.lat = json['coord']['lat'];
    this.lon = json['coord']['lon'];
  }

  @override
  String toString() {
    return this.name;
  }
}
