import 'period.dart';

class Day {
  DateTime date;
  List<Period> dayPeriods = [];
  List<Period> morning;
  List<Period> night;
  Day({
    this.date,
    this.dayPeriods,
  });

  addperiod(Period p) {
    this.dayPeriods.add(p);
  }

  Period findPeriod(DateTime date) {
    List<Duration> durations = [];
    dayPeriods.forEach((dp) {
      durations.add(dp.periodDate.difference(date));
    });
  }

  void setMorningNight() {
    if (dayPeriods.isEmpty) {
      return;
    }
    morning = [];
    night = [];
    dayPeriods.forEach((element) {
      if (element.partOfTheDay == 'd') {
        morning.add(element);
      }
      if (element.partOfTheDay == 'n') {
        night.add(element);
      }
    });
  }

  @override
  String toString() {
    return this.date.toString();
  }
}
