import 'package:dio/dio.dart';
import 'package:flutter/material.dart';

import '../../helpers/loger.dart';

enum CustomErrorType {
  BadInternentConnection,
  NoInternetConnection,
  BadRequest,
  UnAutherized,
  Foribidden,
  InternalServerError,
  Else
}

Map<int, CustomErrorType> httpStatusCodeErrors = {
  400: CustomErrorType.BadRequest,
  401: CustomErrorType.UnAutherized,
  403: CustomErrorType.Foribidden,
  500: CustomErrorType.InternalServerError
};

class CustomError {
  final String message;
  final CustomErrorType type;

  CustomError({@required this.message, @required this.type}) {
    Logger.logError(this.message);
  }

  static CustomError catcher(var error) {
    if (error is CustomError) {
      throw error;
    }

    throw CustomError(
      message: "There is Something wrong , please contact the support",
      type: CustomErrorType.Else,
    );
  }

  static void statusCheck(Response response) {
    if (response.data['cod'] != '200') {
      throw CustomError(
        message: response.data['message'],
        type: CustomErrorType.Else,
      );
    }
  }

  static void networkErrorManager(var error, {String messageKey = 'message'}) {
    String errorMessage = "Something Went Wrong";
    if (error is CustomError) throw error;
    if (error is DioError) {
      if (error.type == DioErrorType.response) {
        errorMessage = error.response.data[messageKey];
        throw CustomError(
          message: errorMessage,
          type: httpStatusCodeErrors[error.response.statusCode],
        );
      }
      if (error.type == DioErrorType.connectTimeout ||
          error.type == DioErrorType.receiveTimeout ||
          error.type == DioErrorType.sendTimeout) {
        errorMessage = "You have a bad Intrenet Connection";
        throw CustomError(
          message: errorMessage,
          type: CustomErrorType.BadInternentConnection,
        );
      }
      if (error.type == DioErrorType.other) {
        errorMessage = "You have no Internet Connection";
        throw CustomError(
          message: errorMessage,
          type: CustomErrorType.NoInternetConnection,
        );
      }
    }
    throw CustomError(message: errorMessage, type: CustomErrorType.Else);
  }

  @override
  String toString() {
    return this.message;
  }
}
