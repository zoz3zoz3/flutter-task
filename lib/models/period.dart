class Period {
  int id;
  DateTime periodDate;

  dynamic temp;
  dynamic feelsLike;
  dynamic tempMin;
  dynamic tempMax;
  dynamic pressure;
  dynamic seaLevel;
  dynamic grndLevel;
  dynamic humidity;
  dynamic temp_kf;

  List<Weather> weather;

  dynamic cloudiness;

  dynamic windSpeed;
  dynamic windDirectionDegrees;
  dynamic windGust;

  dynamic visibility;

  dynamic probabilityOfPrecipitation;

  String partOfTheDay;

  dynamic rain3h;
  dynamic snow3h;

  Period({
    this.id,
    this.cloudiness,
    this.feelsLike,
    this.grndLevel,
    this.humidity,
    this.partOfTheDay,
    this.periodDate,
    this.pressure,
    this.probabilityOfPrecipitation,
    this.rain3h,
    this.seaLevel,
    this.snow3h,
    this.temp,
    this.tempMax,
    this.tempMin,
    this.temp_kf,
    this.visibility,
    this.windDirectionDegrees,
    this.windGust,
    this.windSpeed,
  });

  Period.fromJson(Map<String, dynamic> json) {
    this.id = json['dt'];
    this.temp = json['main']['temp'];
    this.feelsLike = json['main']['feels_like'];
    this.tempMin = json['main']['temp_min'];
    this.tempMax = json['main']['temp_max'];
    this.pressure = json['main']['pressure'];
    this.seaLevel = json['main']['sea_level'];
    this.grndLevel = json['main']['grnd_level'];
    this.humidity = json['main']['humidity'];
    this.temp_kf = json['main']['temp_kf'];

    this.weather = Weather.fromJsonList(json['weather']);

    this.cloudiness = json['clouds']['all'];

    this.windSpeed = json['wind']['speed'];
    this.windDirectionDegrees = json['wind']['deg'];
    this.windGust = json['wind']['gust'];

    this.visibility = json['visibility'];

    this.probabilityOfPrecipitation = json['pop'];

    this.partOfTheDay = json['sys']['pod'];

    this.periodDate = DateTime.parse(json['dt_txt']);
  }

  static List<Period> fromJsonList(List<dynamic> list) {
    if (list == null) return null;
    List<Period> dayPeriods = [];
    list.forEach(
      (item) => {
        dayPeriods.add(Period.fromJson(item)),
      },
    );

    return dayPeriods;
  }

  @override
  String toString() {
    return this.temp.toString() /*this.name*/;
  }
}

class Weather {
  int id;
  String weatherParameters;
  String weatherDescription;
  String icon;

  Weather(
      {this.icon, this.id, this.weatherDescription, this.weatherParameters});

  Weather.fromJson(Map<String, dynamic> json) {
    this.id = json['id'];
    this.weatherParameters = json['main'];
    this.weatherDescription = json['description'];
    this.icon = json['icon'];
  }

  static List<Weather> fromJsonList(List<dynamic> list) {
    if (list == null) return null;
    List<Weather> weathers = [];
    list.forEach(
      (item) => {
        weathers.add(Weather.fromJson(item)),
      },
    );

    return weathers;
  }
}
