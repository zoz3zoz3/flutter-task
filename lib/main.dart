import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:flutter_localizations/flutter_localizations.dart';
import 'localizaion/demo_localization.dart';

import './providers/settings.dart';

import './screens/day-screen.dart';
import './screens/home-screen.dart';
import './screens/splash-screen.dart';
import './screens/settings-screen.dart';

import 'constants/providers.dart';
import 'constants/themes.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await SettingsProvider.setSettings();
  await SettingsProvider.setTheme();
  await SettingsProvider.setUnit();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: providers,
      child: Consumer<SettingsProvider>(
        builder: (ctx, settings, widget) {
          return MaterialApp(
            title: 'Weather app',
            // ignore: missing_return
            onGenerateRoute: (routeSettings) {
              switch (routeSettings.name) {
                case '/':
                  return CupertinoPageRoute(
                    builder: (_) => SplashScreen(),
                  );
                case '/home':
                  return CupertinoPageRoute(
                    builder: (_) => HomeScreen(),
                  );
                case '/day-screen':
                  return CupertinoPageRoute(
                    builder: (_) => DayScreen(),
                  );
                case '/settings-screen':
                  return CupertinoPageRoute(
                    builder: (_) => SettingsScreen(),
                  );
              }
            },
            theme: theme,
            darkTheme: darkTheme,
            themeMode: SettingsProvider.staticDarkMode
                ? ThemeMode.dark
                : ThemeMode.light,
            //home: SplashScreen(),
            debugShowCheckedModeBanner: false,
            supportedLocales: [
              Locale('en', 'US'),
              Locale('ar', 'SY'),
            ],
            localizationsDelegates: [
              DemoLocalizations.delegate,
              GlobalMaterialLocalizations.delegate,
              GlobalWidgetsLocalizations.delegate,
              GlobalCupertinoLocalizations.delegate,
            ],
            localeResolutionCallback: (deviceLocale, supportedLocales) {
              for (var locale in supportedLocales) {
                if (locale.languageCode == deviceLocale.languageCode &&
                    locale.countryCode == deviceLocale.countryCode) {
                  return locale;
                }
              }
              return supportedLocales.first;
            },
            locale: SettingsProvider.staticLocale,
          );
        },
      ),
    );
  }
}
