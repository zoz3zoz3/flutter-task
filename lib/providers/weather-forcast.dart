import 'package:flutter/widgets.dart';
import 'package:dio/dio.dart';

import '../models/common/custom-error.dart';
import '../models/period.dart';
import '../models/day.dart';
import '../models/city.dart';

import '../network/request.dart';
import '../helpers/loger.dart';

class WeatherProvider with ChangeNotifier {
  List<Period> periods = [];
  List<Day> forcastDays = [];
  City city;

  Day selectedDay;

  List<Period> get getallPeriods {
    return periods;
  }

  List<Day> get getalldays {
    return forcastDays;
  }

  City get currentCity {
    return city;
  }

  void setSelected(Day day) {
    this.selectedDay = day;
    //notifyListeners();
  }

  Day get theSlectedDay {
    //notifyListeners();
    return selectedDay;
  }

  Future filterDays() async {
    Logger.debug("filtering days from periods..");

    for (var i = 0;
        i < periods.last.periodDate.day - (periods.first.periodDate.day);
        i++) {
      forcastDays.add(
        Day(
          date: DateTime.now().add(
            Duration(days: i),
          ),
          dayPeriods: periods
              .where(
                (element) =>
                    element.periodDate.day ==
                    DateTime.now()
                        .add(
                          Duration(days: i),
                        )
                        .day,
              )
              .toList(),
        ),
      );
    }
    Logger.debug("done filtering days from periods..!");
  }

  Future<void> fetchData({
    Function(int i, int j) onProgress,
    bool refresh = false,
    String unit,
    String lang,
    Function onComplete,
    Function(CustomError error) onError,
    bool notify = false,
    bool throwOnError = false,
  }) async {
    // note : error handling would be in future builder
    Request request = Request(
      onReceivedProgress: onProgress,
      queryParam: {"units": unit, "lang": lang},
    );
    try {
      Logger.debug("fetching forcast days Periods");
      Logger.debug("and fetching city info");
      Response response = await request.get();
      print(response.data);
      if (refresh) {
        periods.clear();
        forcastDays.clear();
        city = null;
      }
      periods = Period.fromJsonList(response.data['list']);
      if (onComplete != null) onComplete();
      Logger.debug("done fetching forcast Periods ! :) ");

      city = City.fromJson(response.data['city']);
      Logger.debug("done fetching city info ! :) ");
      await filterDays();
      for (var i = 0; i < forcastDays.length; i++) {
        print(forcastDays[i].dayPeriods);
      }
      return;
    } catch (e) {
      if (throwOnError) throw CustomError.catcher(e);
      if (onError != null) onError(CustomError.catcher(e));
    } finally {
      if (notify) notifyListeners();
    }
  }
}
