import 'package:flutter/cupertino.dart';
import '../storage/shared-pref.dart';
import '../helpers/loger.dart';

class SettingsProvider with ChangeNotifier {
  static bool staticDarkMode;
  static String staticUnit;
  static Locale staticLocale;
  String unit = 'metric';
  Locale currlocale = Locale('en', 'US');

  static Future<void> setSettings() async {
    Logger.debug("initializing settings");
    Logger.debug("looking for prefferd lang");
    if (staticLocale != null) return;
    Locale loc;
    String lang = await SharedPref.read('language');
    if (lang == 'en' || lang == null)
      loc = Locale('en', 'US');
    else
      loc = Locale('ar', 'SY');

    staticLocale = loc;

    print(staticLocale);
  }

  static Future<void> setTheme() async {
    Logger.debug("looking for prefered theme");
    if (staticDarkMode != null) return;
    bool status;
    String st = await SharedPref.read('night');
    if (st == 'false' || st == null)
      status = false;
    else
      status = true;

    staticDarkMode = status;
    print(staticDarkMode);
  }

  static Future<void> setUnit() async {
    Logger.debug("looking for prefered unit");
    if (staticUnit != null) return;
    String unit;
    String sunit = await SharedPref.read('unit');
    if (sunit == 'metric' || sunit == null)
      unit = 'metric';
    else
      sunit = 'imperial';

    staticUnit = unit;
    print(staticUnit);
  }

  void changeLocale(Locale locale) async {
    if (locale == staticLocale) return;
    staticLocale = locale;
    currlocale = locale;
    SharedPref.write('language', '${locale.languageCode}');
    notifyListeners();
  }

  void changeTheme(bool dark) {
    if (dark == staticDarkMode) return;
    staticDarkMode = dark;
    SharedPref.write('night', staticDarkMode.toString());
    notifyListeners();
  }

  void changeUnit(String newUnit) {
    if (newUnit == staticUnit) return;
    staticUnit = newUnit;
    unit = newUnit;
    SharedPref.write('unit', staticUnit);
    notifyListeners();
  }
}
